class Shift < ApplicationRecord
  belongs_to :worker, required: false

  validates_presence_of :start_date
end
