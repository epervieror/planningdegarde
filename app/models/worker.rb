class Worker < ApplicationRecord
  has_many :shifts

  enum status: [ :medic, :interne, :interim ]

  validates_presence_of :first_name, :status
end
